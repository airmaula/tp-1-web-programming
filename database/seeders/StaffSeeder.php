<?php

namespace Database\Seeders;

use App\Models\Staff;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class StaffSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (range(1, 6) as $item) {
            Staff::create([
                'name' => fake('id_ID')->name,
                'phone' => fake('id_ID')->phoneNumber,
                'email' => fake('id_ID')->email,
                'role' => $item > 3 ? 'trainer' : 'officer',
                'picture' => 'assets/img/our-team/team-0'. $item+2 .'.jpg',
            ]);
        }
    }
}
