<?php

namespace Database\Seeders;

use App\Models\Plan;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class PlanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $plans = [
            [
                'name' => '5x Pertemuan',
                'description' => 'Senin - Jumat (08:00 - 16:00)',
                'price' => 530_000,
            ],
            [
                'name' => '8x Pertemuan',
                'description' => 'Senin - Jumat (08:00 - 16:00)',
                'price' => 780_000,
            ],
            [
                'name' => '12x Pertemuan',
                'description' => 'Senin - Jumat (08:00 - 16:00)',
                'discount' => 1_060_000 * 0.1,
                'price' => 1_060_000,
            ],
            [
                'name' => '16x Pertemuan',
                'description' => 'Senin - Jumat (08:00 - 16:00)',
                'discount' => 1_350_000 * 0.1,
                'price' => 1_350_000,
            ],

            [
                'name' => '5x Pertemuan',
                'description' => 'Sabtu - Minggu (08:00 - 15:00)',
                'price' => 630_000,
            ],
            [
                'name' => '8x Pertemuan',
                'description' => 'Sabtu - Minggu  (08:00 - 15:00)',
                'price' => 880_000,
            ],
            [
                'name' => '12x Pertemuan',
                'description' => 'Sabtu - Minggu  (08:00 - 15:00)',
                'discount' => 1_160_000 * 0.1,
                'price' => 1_160_000,
            ],
            [
                'name' => '16x Pertemuan',
                'description' => 'Sabtu - Minggu  (08:00 - 15:00)',
                'discount' => 1_450_000 * 0.1,
                'price' => 1_450_000,
            ]
        ];

        foreach ($plans as $plan) {
            Plan::create($plan);
        }

    }
}
