<?php

namespace Database\Seeders;

use App\Models\Schedule;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ScheduleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $schedules = [
            [
                'day' => 1, // Senin
                'start' => '08:00',
                'end' => '16:00',
            ],
            [
                'day' => 2, // Selasa
                'start' => '08:00',
                'end' => '16:00',
            ],
            [
                'day' => 3, // Rabu
                'start' => '08:00',
                'end' => '16:00',
            ],
            [
                'day' => 4, // Kamis
                'start' => '08:00',
                'end' => '16:00',
            ],
            [
                'day' => 5, // Jumat
                'start' => '08:00',
                'end' => '16:00',
            ],
            [
                'day' => 6, // Sabtu
                'start' => '08:00',
                'end' => '16:00',
            ],
            [
                'day' => 7, // Minggu
                'start' => '08:00',
                'end' => '15:00',
            ],
        ];

        foreach ($schedules as $schedule) {
            Schedule::create($schedule);
        }
    }
}
