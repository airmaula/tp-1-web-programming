<?php

namespace Database\Seeders;

use App\Models\Car;
use App\Models\CarBrand;
use App\Models\CarType;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class CarSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $types = [
            [
                'name' => 'Sedan',
                'icon' => 'assets/img/cars/mp-vehicle-03.png',
            ],
            [
                'name' => 'Crossover',
                'icon' => 'assets/img/cars/mp-vehicle-01.png',
            ],
            [
                'name' => 'MPV',
                'icon' => 'assets/img/cars/mp-vehicle-05.png',
            ],
            [
                'name' => 'SUV',
                'icon' => 'assets/img/cars/mp-vehicle-04.png',
            ],
        ];

        foreach ($types as $type) {
            CarType::create($type);
        }

        $brands = [
            [
                'name' => 'Toyota',
                'logo' => '/assets/img/icons/car-icon-04.svg',
            ],
            [
                'name' => 'Honda',
                'logo' => '/assets/img/icons/car-icon-03.svg',
            ],
            [
                'name' => 'Audi',
                'logo' => '/assets/img/icons/car-icon-02.svg',
            ],
            [
                'name' => 'Mercedes-Benz',
                'logo' => '/assets/img/icons/car-icon-01.svg',
            ],
        ];

        foreach ($brands as $brand) {
            CarBrand::create($brand);
        }

        $cars = [
            [
                'name' => 'Toyota Avanza',
                'type_id' => 3,
                'brand_id' => 1,
            ],
            [
                'name' => 'Toyota Yaris',
                'type_id' => 3,
                'brand_id' => 1,
            ],
            [
                'name' => 'Toyota Corolla Altis',
                'type_id' => 1,
                'brand_id' => 1,
            ],


            [
                'name' => 'Honda City',
                'type_id' => 3,
                'brand_id' => 2,
            ],
            [
                'name' => 'Honda Civic',
                'type_id' => 1,
                'brand_id' => 2,
            ],
            [
                'name' => 'Honda CRV',
                'type_id' => 4,
                'brand_id' => 2,
            ],

            [
                'name' => 'Audi A3',
                'type_id' => 1,
                'brand_id' => 3,
            ],
            [
                'name' => 'Audi A4',
                'type_id' => 1,
                'brand_id' => 3,
            ],


            [
                'name' => 'Mercedes-Benz GLA',
                'type_id' => 2,
                'brand_id' => 4,
            ],
            [
                'name' => 'Mercedes-Benz CLA',
                'type_id' => 1,
                'brand_id' => 4,
            ],
        ];

        foreach ($cars as $car) {
            Car::create([
                'name' => $car['name'],
                'type_id' => $car['type_id'],
                'brand_id' => $car['brand_id'],
                'transmission' => fake()->randomElement(['matic', 'manual']),
                'picture' => 'assets/img/cars/' . Str::slug($car['name']) . '.png',
                'stock' => fake()->numberBetween(10, 20),
            ]);
        }
    }
}
