<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <title>Irfan Drive Pro</title>

    <link rel="shortcut icon" href="{{ asset('/') }}assets/img/favicon.png">

    <link rel="stylesheet" href="{{ asset('/') }}assets/css/bootstrap.min.css">

    <link rel="stylesheet" href="{{ asset('/') }}assets/plugins/fontawesome/css/fontawesome.min.css">
    <link rel="stylesheet" href="{{ asset('/') }}assets/plugins/fontawesome/css/all.min.css">

    <link rel="stylesheet" href="{{ asset('/') }}assets/plugins/select2/css/select2.min.css">

    <link rel="stylesheet" href="{{ asset('/') }}assets/css/bootstrap-datetimepicker.min.css">

    <link rel="stylesheet" href="{{ asset('/') }}assets/plugins/aos/aos.css">

    <link rel="stylesheet" href="{{ asset('/') }}assets/css/feather.css">

    <link rel="stylesheet" href="{{ asset('/') }}assets/css/owl.carousel.min.css">

    <link rel="stylesheet" href="{{ asset('/') }}assets/css/style.css">
</head>
<body>
<div class="main-wrapper">

    <header class="header">
        <div class="container-fluid">
            <nav class="navbar navbar-expand-lg header-nav">
                <div class="navbar-header">
                    <a id="mobile_btn" href="javascript:void(0);">
                    <span class="bar-icon">
                    <span></span>
                    <span></span>
                    <span></span>
                    </span>
                    </a>
                    <a href="{{route('homepage')}}" class="navbar-brand logo d-flex align-items-center">
                        <img src="{{ asset('/') }}assets/img/logo-small.png" class="img-fluid" style="margin-top: -0.5rem" alt="Logo"> <h3 class="ms-3 mb-0"><span class="text-primary">IRFAN</span> DRIVE PRO</h3>
                    </a>
                    <a href="{{route('homepage')}}" class="navbar-brand logo-small">
                        <img src="{{ asset('/') }}assets/img/logo-small.png" class="img-fluid" alt="Logo">
                    </a>
                </div>
                <div class="main-menu-wrapper">
                    <div class="menu-header">
                        <a href="{{route('homepage')}}" class="menu-logo">
                            <img src="{{ asset('/') }}assets/img/logo.svg" class="img-fluid" alt="Logo">
                        </a>
                        <a id="menu_close" class="menu-close" href="javascript:void(0);"> <i class="fas fa-times"></i></a>
                    </div>
                    <ul class="main-nav">
                        <li @class(['active' => \Request::route()->getName() == 'homepage'])><a href="{{route('homepage')}}">Beranda</a></li>
                        <li @class(['active' => \Request::route()->getName() == 'schedule'])><a href="{{route('schedule')}}">Jadwal Pelatihan</a></li>
                        <li @class(['active' => \Request::route()->getName() == 'plan'])><a href="{{route('plan')}}">Penawaran Menarik</a></li>
                        <li @class(['active' => \Request::route()->getName() == 'cars'])><a href="{{route('cars')}}">Pilihan Mobil</a></li>
                        <li @class(['active' => \Request::route()->getName() == 'staff'])><a href="{{route('staff')}}">Tim Kami</a></li>
                        <li @class(['active' => \Request::route()->getName() == 'contact'])><a href="{{route('contact')}}">Kontak</a></li>
                    </ul>
                </div>
                <ul class="nav header-navbar-rht">
                    <li class="nav-item">
                        <a class="nav-link header-reg" target="_blank" href="https://wa.me/6289503559403?text=Halo min, saya ingin daftar kursus mengemudi."><span><i class="fa-solid fa-user"></i></span>Daftar Sekarang</a>
                    </li>
                </ul>
            </nav>
        </div>
    </header>


    @yield('content')


    <footer class="footer">

        <div class="footer-top aos" data-aos="fade-up">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="footer-contact footer-widget">
                            <div class="row">
                                <div class="col-md-3">
                                    <h5 class="footer-title">Kontak Kami</h5>
                                    <div class="footer-contact-info">
                                        <div class="footer-address">
                                            <span><i class="fa fa-phone"></i></span>
                                            <div class="addr-info">
                                                <a href="tel:+1(888)7601940">+ 62 821 2585 7754</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <h5 class="footer-title">Alamat</h5>
                                    <div class="footer-contact-info">
                                        <div class="footer-address">
                                            <span><i class="fa fa-map-location"></i></span>
                                            <div class="addr-info">
                                                <a href="tel:+1(888)7601940">
                                                    Jl. Raya Cibogo No.20 Subang, Jawa Barat, Indonesia
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <h5 class="footer-title">Newsletter</h5>
                                    <div class="footer-contact-info">
                                        <div class="update-form">
                                            <form action="#">
                                                <span><i class="fa fa-envelope"></i></span>
                                                <input type="email" class="form-control" placeholder="Masukan email kamu disini">
                                                <button type="submit" class="btn btn-subscribe"><span><i class="fa fa-paper-plane"></i></span></button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="footer-bottom">
            <div class="container">

                <div class="copyright">
                    <div class="row align-items-center">
                        <div class="col-md-12 text-center">
                            <div class="copyright-text">
                                <p>© 2023 Ahmad Irfan Maulana (2401983161), Tugas Personal 1 - Web Programming.</p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </footer>

</div>

<div class="progress-wrap active-progress">
    <svg class="progress-circle svg-content" width="100%" height="100%" viewbox="-1 -1 102 102">
        <path d="M50,1 a49,49 0 0,1 0,98 a49,49 0 0,1 0,-98" style="transition: stroke-dashoffset 10ms linear 0s; stroke-dasharray: 307.919px, 307.919px; stroke-dashoffset: 228.265px;"></path>
    </svg>
</div>



<script src="{{ asset('/') }}assets/js/jquery-3.6.4.min.js"></script>
<script src="{{ asset('/') }}assets/js/bootstrap.bundle.min.js"></script>

<script src="{{ asset('/') }}assets/js/jquery.waypoints.js"></script>
<script src="{{ asset('/') }}assets/js/jquery.counterup.min.js"></script>

<script src="{{ asset('/') }}assets/plugins/select2/js/select2.min.js"></script>

<script src="{{ asset('/') }}assets/plugins/aos/aos.js"></script>

<script src="{{ asset('/') }}assets/js/backToTop.js"></script>

<script src="{{ asset('/') }}assets/plugins/moment/moment.min.js"></script>
<script src="{{ asset('/') }}assets/js/bootstrap-datetimepicker.min.js"></script>

<script src="{{ asset('/') }}assets/js/owl.carousel.min.js"></script>

<script src="{{ asset('/') }}assets/js/script.js"></script>
</body>
</html>
