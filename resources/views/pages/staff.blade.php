@extends('layouts.app')

@section('content')
    <section class="our-team-section">
        <div class="container">
            @foreach($staff as $role => $data)
                <div class="mb-5">
                    <div class="section-heading aos-init aos-animate" data-aos="fade-down">
                        <h2>{{['officer' => 'Pengurus', 'trainer' => 'Instruktur'][$role]}}</h2>
                    </div>
                    <div class="row justify-content-center">
                        @foreach($data as $team)
                            <div class="col-lg-3 col-md-6 col-12 aos-init aos-animate" data-aos="fade-down" data-aos-duration="1200" data-aos-delay="100">
                                <div class="our-team">
                                    <div class="profile-pic">
                                        <img src="{{asset($team->picture)}}" alt="Our Team">
                                    </div>
                                    <div class="team-prof">
                                        <h6 class="mb-3">{{$team->name}}</h6>
                                        <div class="footer-social-links m-0">
                                            <ul class="nav">
                                                <li class="mb-1">
                                                    <i class="fa fa-phone ms-2"></i> {{$team->phone}}
                                                </li>
                                                <li>
                                                    <i class="fa fa-envelope ms-2"></i> {{$team->email}}
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            @endforeach
        </div>
    </section>
@endsection
