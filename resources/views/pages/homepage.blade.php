
@extends('layouts.app')

@section('content')

    <!-- S: Banner -->
    <section class="banner-section banner-slider">
        <div class="container">
            <div class="home-banner">
                <div class="row align-items-center">
                    <div class="col-lg-6" data-aos="fade-down">
                        <p class="explore-text"> <span><i class="fa-solid fa-thumbs-up me-2"></i></span>100% Pelatihan Menyetir Mobil Terpercaya di Indonesia</p>
                        <h1>Kendalikan Jalan <br>
                            <span>dengan Keahlian Anda</span></h1>
                        <p>Kuasai Kemampuan Mengemudi dengan Percaya Diri dan Profesionalisme di Pelatihan Menyetir Mobil Kami!</p>
                        <div class="view-all">
                            <a href="{{route('schedule')}}" class="btn btn-view d-inline-flex align-items-center">Lihat Jadwal Pelatihan <span><i class="fa fa-chevron-right ms-2"></i></span></a>
                        </div>
                    </div>
                    <div class="col-lg-6" data-aos="fade-down">
                        <div class="banner-imgs">
                            <img src="{{ asset('/') }}assets/img/cars/honda-civic.png" class="img-fluid aos" alt="bannerimage">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- E: Banner -->

    <!-- S: Services -->
    <section class="section services">
        <div class="service-right">
            <img src="{{ asset('/') }}assets/img/bg/service-right.svg" class="img-fluid" alt="services right">
        </div>
        <div class="container">

            <div class="section-heading" data-aos="fade-down">
                <h2>Bagaimana Cara Kerjanya?</h2>
                <p>Pahami langkah-langkahnya dan menjadi seorang pengemudi yang terampil!</p>
            </div>

            <div class="services-work">
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-12" data-aos="fade-down">
                        <div class="services-group">
                            <div class="services-icon border-secondary">
                                <img class="icon-img bg-secondary" src="{{ asset('/') }}assets/img/icons/services-icon-01.svg" alt="Choose Locations">
                            </div>
                            <div class="services-content">
                                <h3>1. Pendaftaran</h3>
                                <p>Daftarlah ke kursus mengemudi kami dengan mengisi formulir pendaftaran online atau menghubungi tim kami.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-12" data-aos="fade-down">
                        <div class="services-group">
                            <div class="services-icon border-warning">
                                <img class="icon-img bg-warning" src="{{ asset('/') }}assets/img/icons/services-icon-01.svg" alt="Choose Locations">
                            </div>
                            <div class="services-content">
                                <h3>2. Pembelajaran Teori</h3>
                                <p>Mulailah dengan mempelajari teori dasar mengemudi melalui modul pembelajaran interaktif kami. </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-12" data-aos="fade-down">
                        <div class="services-group">
                            <div class="services-icon border-dark">
                                <img class="icon-img bg-dark" src="{{ asset('/') }}assets/img/icons/services-icon-01.svg" alt="Choose Locations">
                            </div>
                            <div class="services-content">
                                <h3>3. Praktik Mengemudi</h3>
                                <p>Kami akan mengatur sesi pelatihan praktik mengemudi dengan bimbingan instruktur berpengalaman.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- E: Services -->

    <!-- S: Car List -->
    <section class="section popular-services popular-explore">
        <div class="container">

            <div class="section-heading" data-aos="fade-down">
                <h2>Jelajahi Mobil - Mobil Terpopuler</h2>
                <p>Pilih Kendaraan Impian Anda dari Beragam Pilihan Mobil Populer di Irfan Drive Pro</p>
            </div>

            <div class="row justify-content-center">
                <div class="col-lg-12" data-aos="fade-down">
                    <div class="listing-tabs-group">
                        <ul class="nav listing-buttons gap-3" data-bs-tabs="tabs">
                            @foreach($brands as $i => $brand)
                                <li>
                                    <a class="{{$i==0 ? 'active' : ''}}" aria-current="true" data-bs-toggle="tab" href="#brand-{{$i+1}}">
                                    <span>
                                    <img src="{{ asset($brand->logo) }}" alt="{{$brand->name}}">
                                    </span>
                                        {{$brand->name}}
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
            <div class="tab-content">
                @foreach($brands as $i => $brand)
                    <div class="tab-pane {{$i==0 ? 'active' : ''}}" id="brand-{{$i+1}}">
                        <div class="row">

                            @foreach($brand->cars as $car)
                                <div class="col-lg-4 col-md-6 col-12" data-aos="fade-down">
                                    <div class="listing-item">
                                        <div class="listing-img bg-light pt-4">
                                            <a href="">
                                                <img src="{{ asset($car->picture) }}" class="img-fluid" alt="{{$car->name}}" style="height: 250px; object-fit: contain; object-position: center">
                                            </a>
                                            <div class="fav-item">
                                                <span class="featured-text text-capitalize">{{$brand->name}}</span>
                                            </div>
                                        </div>
                                        <div class="listing-content">
                                            <div class="listing-features">
                                                <h3 class="listing-title">
                                                    <a href="#">{{$car->name}}</a>
                                                </h3>
                                            </div>
                                            <div class="listing-details-group">
                                                <ul>
                                                    <li>
                                                        <span><img src="{{ asset('/') }}assets/img/icons/car-parts-01.svg" alt="Diesel"></span>
                                                        <p class="text-capitalize">{{$car->transmission}}</p>
                                                    </li>
                                                    <li>
                                                        <span><img src="{{ asset('/') }}assets/img/icons/car-parts-05.svg" alt="2018"></span>
                                                        <p>2018</p>
                                                    </li>
                                                    <li>
                                                        <span><img src="{{ asset('/') }}assets/img/icons/car-parts-04.svg" alt="Petrol"></span>
                                                        <p>{{$car->type->name}}</p>
                                                    </li>
                                                    <li>
                                                        <span><img src="{{ asset('/') }}assets/img/icons/car-parts-06.svg" alt="Petrol"></span>
                                                        <p>{{$car->stock}} Mobil</p>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach

                        </div>
                    </div>
                @endforeach
            </div>

            <div class="text-center mt-3">
                <a href="{{route('cars')}}" class="btn btn-view d-inline-flex align-items-center">Lihat Semua Mobil <span><i class="fa fa-chevron-right ms-2"></i></span></a>
            </div>

        </div>
    </section>
    <!-- E: Car List -->

    <!-- S: Car Type -->
    <section class="section popular-car-type">
        <div class="container">

            <div class="section-heading" data-aos="fade-down">
                <h2>Tipe Mobil Terpopuler</h2>
                <p>Temukan Tipe Mobil Paling Diminati</p>
            </div>

            <div class="row">
                <div class="popular-slider-group">
                    <div class="owl-carousel popular-cartype-slider owl-theme">

                        @foreach($types as $type)
                            <div class="listing-owl-item">
                                <div class="listing-owl-group">
                                    <div class="listing-owl-img">
                                        <img src="{{ asset($type->icon) }}" class="img-fluid" alt="{{$type->name}}">
                                    </div>
                                    <h6>{{$type->name}}</h6>
                                    <p>{{$type->cars->count()}} Cars</p>
                                </div>
                            </div>
                        @endforeach

                    </div>
                </div>
            </div>

        </div>
    </section>
    <!-- E: Car Type -->

    <!-- S: Why Choose Us -->
    <section class="section why-choose popular-explore">
        <div class="choose-left">
            <img src="{{ asset('/') }}assets/img/bg/choose-left.png" class="img-fluid" alt="Why Choose Us">
        </div>
        <div class="container">

            <div class="section-heading" data-aos="fade-down">
                <h2>Kenapa Memilih Kami?</h2>
                <p>Percayakan kepada ahlinya merupakan alasan mengapa kami pilihan terbaik untuk kursus menyetir mobil</p>
            </div>

            <div class="why-choose-group">
                <div class="row">
                    <div class="col-lg-4 col-md-6 col-12 d-flex" data-aos="fade-down">
                        <div class="card flex-fill">
                            <div class="card-body">
                                <div class="choose-img choose-black">
                                    <img src="{{ asset('/') }}assets/img/icons/bx-user-check.svg" alt>
                                </div>
                                <div class="choose-content">
                                    <h4>Instruktur Berpengalaman</h4>
                                    <p>Tim instruktur kami terdiri dari para profesional berpengalaman yang ahli dalam pelatihan menyetir mobil.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-12 d-flex" data-aos="fade-down">
                        <div class="card flex-fill">
                            <div class="card-body">
                                <div class="choose-img choose-secondary">
                                    <img src="{{ asset('/') }}assets/img/icons/bx-user-check.svg" alt>
                                </div>
                                <div class="choose-content">
                                    <h4>Kualitas dan Keamanan</h4>
                                    <p>Kami mengutamakan kualitas dan keamanan dalam setiap aspek pelatihan kami, termasuk kendaraan yang berkualitas dan dilengkapi dengan fitur keamanan terkini.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-12 d-flex" data-aos="fade-down">
                        <div class="card flex-fill">
                            <div class="card-body">
                                <div class="choose-img choose-primary">
                                    <img src="{{ asset('/') }}assets/img/icons/bx-user-check.svg" alt>
                                </div>
                                <div class="choose-content">
                                    <h4>Harga Kompetitif</h4>
                                    <p>Kami menawarkan harga yang kompetitif dan terjangkau untuk pelatihan menyetir mobil berkualitas tinggi.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- E: Why Choose Us -->

    <!-- S: Testimonial -->
    <section class="section about-testimonial testimonials-section">
        <div class="container">

            <div class="section-heading" data-aos="fade-down">
                <h2 class="title text-white">Apa Kata Mereka Tentang Kami? </h2>
                <p class="description text-white">Pendapat peserta tentang layanan kami</p>
            </div>

            <div class="owl-carousel about-testimonials testimonial-group mb-0 owl-theme">

                <div class="testimonial-item d-flex">
                    <div class="card flex-fill">
                        <div class="card-body">
                            <div class="quotes-head"></div>
                            <div class="review-box">
                                <div class="review-profile">
                                    <div class="review-img">
                                        <img src="{{ asset('/') }}assets/img/profiles/avatar-02.jpg" class="img-fluid" alt="img">
                                    </div>
                                </div>
                                <div class="review-details">
                                    <h6>Ryan</h6>
                                    <div class="list-rating">
                                        <div class="list-rating-star">
                                            <i class="fas fa-star filled"></i>
                                            <i class="fas fa-star filled"></i>
                                            <i class="fas fa-star filled"></i>
                                            <i class="fas fa-star filled"></i>
                                            <i class="fas fa-star filled"></i>
                                        </div>
                                        <p><span>(5.0)</span></p>
                                    </div>
                                </div>
                            </div>
                            <p>Kursus menyetir di sini benar-benar mengubah cara pandang saya tentang mengemudi. Saya sekarang merasa lebih percaya diri dan memiliki pemahaman yang lebih baik tentang aturan lalu lintas. Terima kasih atas pengalaman yang luar biasa!</p>
                        </div>
                    </div>
                </div>


                <div class="testimonial-item d-flex">
                    <div class="card flex-fill">
                        <div class="card-body">
                            <div class="quotes-head"></div>
                            <div class="review-box">
                                <div class="review-profile">
                                    <div class="review-img">
                                        <img src="{{ asset('/') }}assets/img/profiles/avatar-03.jpg" class="img-fluid" alt="img">
                                    </div>
                                </div>
                                <div class="review-details">
                                    <h6>Amanda</h6>
                                    <div class="list-rating">
                                        <div class="list-rating-star">
                                            <i class="fas fa-star filled"></i>
                                            <i class="fas fa-star filled"></i>
                                            <i class="fas fa-star filled"></i>
                                            <i class="fas fa-star filled"></i>
                                            <i class="fas fa-star filled"></i>
                                        </div>
                                        <p><span>(5.0)</span></p>
                                    </div>
                                </div>
                            </div>
                            <p>Saya sangat senang dengan pengalaman kursus menyetir di sini. Instruktur yang ramah dan sabar membantu saya memperoleh kepercayaan diri dalam mengemudi. Terima kasih banyak!</p>
                        </div>
                    </div>
                </div>


                <div class="testimonial-item d-flex">
                    <div class="card flex-fill">
                        <div class="card-body">
                            <div class="quotes-head"></div>
                            <div class="review-box">
                                <div class="review-profile">
                                    <div class="review-img">
                                        <img src="{{ asset('/') }}assets/img/profiles/avatar-04.jpg" class="img-fluid" alt="img">
                                    </div>
                                </div>
                                <div class="review-details">
                                    <h6>Jessica</h6>
                                    <div class="list-rating">
                                        <div class="list-rating-star">
                                            <i class="fas fa-star filled"></i>
                                            <i class="fas fa-star filled"></i>
                                            <i class="fas fa-star filled"></i>
                                            <i class="fas fa-star filled"></i>
                                            <i class="fas fa-star filled"></i>
                                        </div>
                                        <p><span>(5.0)</span></p>
                                    </div>
                                </div>
                            </div>
                            <p>Pelatihan yang saya terima di sini sungguh luar biasa. Mereka memberikan penekanan pada keamanan berkendara dan memberikan wawasan yang sangat berharga. Saya merasa siap untuk menghadapi jalan raya setelah kursus ini.</p>
                        </div>
                    </div>
                </div>


                <div class="testimonial-item d-flex">
                    <div class="card flex-fill">
                        <div class="card-body">
                            <div class="quotes-head"></div>
                            <div class="review-box">
                                <div class="review-profile">
                                    <div class="review-img">
                                        <img src="{{ asset('/') }}assets/img/profiles/avatar-06.jpg" class="img-fluid" alt="img">
                                    </div>
                                </div>
                                <div class="review-details">
                                    <h6>Lisa</h6>
                                    <div class="list-rating">
                                        <div class="list-rating-star">
                                            <i class="fas fa-star filled"></i>
                                            <i class="fas fa-star filled"></i>
                                            <i class="fas fa-star filled"></i>
                                            <i class="fas fa-star filled"></i>
                                            <i class="fas fa-star filled"></i>
                                        </div>
                                        <p><span>(5.0)</span></p>
                                    </div>
                                </div>
                            </div>
                            <p>Saya telah mencoba beberapa kursus menyetir sebelumnya, tetapi kursus di tempat ini benar-benar berbeda. Instruktur yang berpengalaman dan pendekatan praktis mereka membuat saya merasa nyaman dan menguasai keterampilan mengemudi dengan cepat.</p>
                        </div>
                    </div>
                </div>


                <div class="testimonial-item d-flex">
                    <div class="card flex-fill">
                        <div class="card-body">
                            <div class="quotes-head"></div>
                            <div class="review-box">
                                <div class="review-profile">
                                    <div class="review-img">
                                        <img src="{{ asset('/') }}assets/img/profiles/avatar-07.jpg" class="img-fluid" alt="img">
                                    </div>
                                </div>
                                <div class="review-details">
                                    <h6>David</h6>
                                    <div class="list-rating">
                                        <div class="list-rating-star">
                                            <i class="fas fa-star filled"></i>
                                            <i class="fas fa-star filled"></i>
                                            <i class="fas fa-star filled"></i>
                                            <i class="fas fa-star filled"></i>
                                            <i class="fas fa-star filled"></i>
                                        </div>
                                        <p><span>(5.0)</span></p>
                                    </div>
                                </div>
                            </div>
                            <p>Saya sangat merekomendasikan kursus ini kepada siapa pun yang ingin belajar menyetir. Instruktur yang terampil, materi yang relevan, dan lingkungan belajar yang kondusif membuat pengalaman belajar menjadi menyenangkan dan efektif.</p>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <!-- E: Testimonial -->

    <!-- S: FAQ -->
    <section class="section faq-section bg-light-primary">
        <div class="container">

            <div class="section-heading" data-aos="fade-down">
                <h2>Pertanyaan yang Sering Diajukan </h2>
                <p>Temukan Jawaban untuk Pertanyaan-pertanyaan Umum Mengenai Kursus Menyetir Mobil Kami</p>
            </div>

            <div class="faq-info">
                <div class="faq-card bg-white" data-aos="fade-down">
                    <h4 class="faq-title">
                        <a class="collapsed" data-bs-toggle="collapse" href="#faqOne" aria-expanded="false">Apakah saya perlu memiliki SIM sebelum mengikuti kursus?</a>
                    </h4>
                    <div id="faqOne" class="card-collapse collapse">
                        <p>Ya, untuk mengikuti kursus menyetir mobil, Anda harus memiliki Surat Izin Mengemudi (SIM) yang valid sesuai dengan peraturan setempat.</p>
                    </div>
                </div>
                <div class="faq-card bg-white" data-aos="fade-down">
                    <h4 class="faq-title">
                        <a class="collapsed" data-bs-toggle="collapse" href="#faqTwo" aria-expanded="false">Berapa lama kursus menyetir mobil ini berlangsung?</a>
                    </h4>
                    <div id="faqTwo" class="card-collapse collapse">
                        <p>Durasi kursus menyetir mobil kami bervariasi tergantung pada jenis program yang Anda pilih. Biasanya, kursus dapat berlangsung antara beberapa minggu hingga beberapa bulan.</p>
                    </div>
                </div>
                <div class="faq-card bg-white" data-aos="fade-down">
                    <h4 class="faq-title">
                        <a class="collapsed" data-bs-toggle="collapse" href="#faqThree" aria-expanded="false">Apakah kursus menyetir mobil ini cocok untuk pemula?</a>
                    </h4>
                    <div id="faqThree" class="card-collapse collapse">
                        <p>Ya, kursus menyetir mobil kami cocok untuk pemula. Instruktur kami akan memberikan pelatihan yang sesuai dengan tingkat keahlian Anda, sehingga Anda dapat memulai dari dasar-dasar mengemudi.</p>
                    </div>
                </div>
                <div class="faq-card bg-white" data-aos="fade-down">
                    <h4 class="faq-title">
                        <a class="collapsed" data-bs-toggle="collapse" href="#faqFour" aria-expanded="false">Apakah kendaraan yang digunakan dalam kursus adalah kendaraan manual atau otomatis?</a>
                    </h4>
                    <div id="faqFour" class="card-collapse collapse">
                        <p>Kami menyediakan kursus untuk kedua jenis kendaraan, baik kendaraan manual maupun otomatis. Anda dapat memilih jenis kendaraan yang ingin Anda pelajari saat mendaftar.</p>
                    </div>
                </div>
                <div class="faq-card bg-white" data-aos="fade-down">
                    <h4 class="faq-title">
                        <a class="collapsed" data-bs-toggle="collapse" href="#faqFive" aria-expanded="false">Apakah ada ujian yang harus saya lalui setelah menyelesaikan kursus?</a>
                    </h4>
                    <div id="faqFive" class="card-collapse collapse">
                        <p>Ya, setelah menyelesaikan kursus menyetir mobil, Anda akan mengikuti ujian praktik yang melibatkan pengemudian di jalan raya. Ujian ini akan mengevaluasi kemampuan Anda dalam mengemudi dengan aman dan mematuhi peraturan lalu lintas.</p>
                    </div>
                </div>
                <div class="faq-card bg-white" data-aos="fade-down">
                    <h4 class="faq-title">
                        <a class="collapsed" data-bs-toggle="collapse" href="#faqSix" aria-expanded="false">Apakah ada dukungan setelah menyelesaikan kursus?</a>
                    </h4>
                    <div id="faqSix" class="card-collapse collapse">
                        <p>Ya, kami menyediakan dukungan pasca-kursus. Setelah menyelesaikan kursus, Anda dapat menghubungi kami untuk konsultasi atau bantuan tambahan terkait dengan mengemudi.</p>
                    </div>
                </div>
                <div class="faq-card bg-white" data-aos="fade-down">
                    <h4 class="faq-title">
                        <a class="collapsed" data-bs-toggle="collapse" href="#faqSeven" aria-expanded="false">Apa yang harus saya bawa saat mengikuti kursus menyetir?</a>
                    </h4>
                    <div id="faqSeven" class="card-collapse collapse">
                        <p>Anda perlu membawa SIM yang valid, kartu identitas, dan semangat belajar yang tinggi. Kami akan memberikan informasi lebih lanjut tentang persyaratan dan persiapan yang perlu Anda lakukan sebelum kursus dimulai.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- E: FAQ -->

    <!-- S: Plan -->
    <section class="pricing-section pricing-page pricing-section-bottom">
        <div class="container">

            <div class="section-heading" data-aos="fade-down">
                <h2>Harga Yang Transparan Untuk Anda</h2>
                <p>Ketahui dengan Jelas Harga dan Paket Kursus Kami</p>
            </div>

            <div class="row">
                @foreach($plans->slice(0, 4) as $i => $plan)
                    <div class="col-lg-3 d-flex col-md-6 col-12" data-aos="fade-down">
                        <div class="price-card flex-fill">
                            <div class="price-head">
                                <div class="price-level {{$i == 2 || $i == 6 ? 'price-level-popular' : ''}}">
                                    <h6>{{$plan->name}}</h6>
                                    <p>{{$plan->description}}</p>
                                </div>
                                <h4>Rp {{number_format($plan->price - $plan->discount, null, ',', '.')}}</h4>
                                @if($plan->discount)
                                    <span class="d-flex"><del class="me-2"><h5>Rp {{number_format($plan->price, null, ',', '.')}}</h5></del> (Diskon : {{$plan->discount/$plan->price * 100}}%)</span>
                                @else
                                    <span>-</span>
                                @endif
                            </div>
                            <div class="price-details">
                                <ul>
                                    <li class="price-check"><span><i class="fa-regular fa-circle-check"></i></span>Semua Tipe Kendaraan</li>
                                    <li class="price-check"><span><i class="fa-regular fa-circle-check"></i></span>Asuransi Kecelakaan</li>
                                    <li class="price-check"><span><i class="fa-regular fa-circle-check"></i></span>Instruktur Yang Berpengalaman</li>
                                    <li class="price-check"><span><i class="fa-regular fa-circle-check"></i></span>Kendaraan Yang Aman dan Terawat</li>
                                    <li class="price-check"><span><i class="fa-regular fa-circle-check"></i></span>Surat Izin Mengemudi (SIM)</li>
                                    <li class="price-check"><span><i class="fa-regular fa-circle-check"></i></span>Sertifikat Mengemudi</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>

            <div class="text-center mt-3">
                <a href="{{route('plan')}}" class="btn btn-view d-inline-flex align-items-center">Lihat Selengkapnya <span><i class="fa fa-chevron-right ms-2"></i></span></a>
            </div>

        </div>
    </section>
    <!-- E: Plan -->

@endsection
