@extends('layouts.app')

@section('content')
    <section class="pricing-section pricing-page pricing-section-bottom">
        <div class="container">

            <div class="section-heading" data-aos="fade-down">
                <h2>Penawaran Menarik Untuk Anda</h2>
                <p>Ketahui dengan Jelas Harga dan Paket Kursus Kami</p>
            </div>

            <div class="row">
                @foreach($plans as $i => $plan)
                    <div class="col-lg-3 d-flex col-md-6 col-12" data-aos="fade-down">
                        <div class="price-card flex-fill">
                            <div class="price-head">
                                <div class="price-level {{$i == 2 || $i == 6 ? 'price-level-popular' : ''}}">
                                    <h6>{{$plan->name}}</h6>
                                    <p>{{$plan->description}}</p>
                                </div>
                                <h4>Rp {{number_format($plan->price - $plan->discount, null, ',', '.')}}</h4>
                                @if($plan->discount)
                                    <span class="d-flex"><del class="me-2"><h5>Rp {{number_format($plan->price, null, ',', '.')}}</h5></del> (Diskon : {{$plan->discount/$plan->price * 100}}%)</span>
                                @else
                                    <span>-</span>
                                @endif
                            </div>
                            <div class="price-details">
                                <ul>
                                    <li class="price-check"><span><i class="fa-regular fa-circle-check"></i></span>Semua Tipe Kendaraan</li>
                                    <li class="price-check"><span><i class="fa-regular fa-circle-check"></i></span>Asuransi Kecelakaan</li>
                                    <li class="price-check"><span><i class="fa-regular fa-circle-check"></i></span>Instruktur Yang Berpengalaman</li>
                                    <li class="price-check"><span><i class="fa-regular fa-circle-check"></i></span>Kendaraan Yang Aman dan Terawat</li>
                                    <li class="price-check"><span><i class="fa-regular fa-circle-check"></i></span>Surat Izin Mengemudi (SIM)</li>
                                    <li class="price-check"><span><i class="fa-regular fa-circle-check"></i></span>Sertifikat Mengemudi</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>

        </div>
    </section>
@endsection
