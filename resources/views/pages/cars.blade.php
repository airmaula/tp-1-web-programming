@extends('layouts.app')

@section('content')
    <div class="breadcrumb-bar">
        <div class="container">
            <div class="row align-items-center text-center">
                <div class="col-md-12 col-12">
                    <h2 class="breadcrumb-title">Pilihan Mobil</h2>
                    <nav aria-label="breadcrumb" class="page-breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{route('homepage')}}">Beranda</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Pilihan Mobil</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>

    <section class="section car-listing">
        <div class="container">
            <div class="row">
                <div class="col-xl-3 col-lg-4 col-sm-12 col-12 theiaStickySidebar">
                    <form action="" autocomplete="off" class="sidebar-form">

                        <div class="sidebar-heading">
                            <h3>Cari Mobil</h3>
                        </div>
                        <div class="product-search">
                            <div class="form-custom">
                                <input type="text" class="form-control" id="member_search1" name="keyword" value="{{request()->get('keyword')}}" placeholder>
                                <span><img src="{{asset('/')}}assets/img/icons/search.svg" alt="img"></span>
                            </div>
                        </div>
                        <div class="accordion" id="accordionMain1">
                            <div class="card-header-new" id="headingOne">
                                <h6 class="filter-title">
                                    <a href="javascript:void(0);" class="w-100" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        Kategori Mobil
                                        <span class="float-end"><i class="fa-solid fa-chevron-down"></i></span>
                                    </a>
                                </h6>
                            </div>
                            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample1">
                                <div class="card-body-chat">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div id="checkBoxes1">
                                                <div class="selectBox-cont">
                                                    @foreach($brands as $brand)
                                                        <label class="custom_check w-100">
                                                            <input type="checkbox" name="brands[]" value="{{$brand->id}}"  @checked(request()->get('brands') && in_array($brand->id, request()->get('brands')))>
                                                            <span class="checkmark"></span> {{$brand->name}}
                                                        </label>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="accordion" id="accordionMain2">
                            <div class="card-header-new" id="headingTwo">
                                <h6 class="filter-title">
                                    <a href="javascript:void(0);" class="w-100" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                                        Tipe Mobil
                                        <span class="float-end"><i class="fa-solid fa-chevron-down"></i></span>
                                    </a>
                                </h6>
                            </div>
                            <div id="collapseTwo" class="collapse show" aria-labelledby="headingTwo" data-bs-parent="#accordionExample2">
                                <div class="card-body-chat">
                                    <div id="checkBoxes2">
                                        <div class="selectBox-cont">
                                            @foreach($types as $type)
                                                <label class="custom_check w-100">
                                                    <input type="checkbox" name="types[]" value="{{$type->id}}" @checked(request()->get('types') && in_array($type->id, request()->get('types')))>
                                                    <span class="checkmark"></span> {{$type->name}}
                                                </label>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="d-inline-flex align-items-center justify-content-center btn w-100 btn-primary filter-btn">
                            <span><i class="fa fa-filter me-2"></i></span>Filter Hasil
                        </button>
                        <a href="{{route('cars')}}" class="reset-filter">Reset Filter</a>
                    </form>
                </div>
                <div class="col-xl-9 col-lg-8 col-sm-12 col-12">
                    <div class="row">
                        @if($cars->count() == 0)
                            <h6 class="text-center">Tidak ada hasil</h6>
                        @endif
                        @foreach($cars as $car)
                            <div class="listview-car">
                                <div class="card">
                                    <div class="blog-widget d-flex">
                                        <div class="blog-img bg-light p-2">
                                            <a href="#">
                                                <img src="{{asset($car->picture)}}" class="img-fluid" alt="blog-img" style="width: 300px">
                                            </a>
                                        </div>
                                        <div class="bloglist-content w-100">
                                            <div class="card-body">
                                                <div class="blog-list-head d-flex">
                                                    <div class="blog-list-title">
                                                        <h3><a href="#">{{$car->name}}</a></h3>
                                                    </div>
                                                </div>
                                                <div class="listing-details-group">
                                                    <ul>
                                                        <li>
                                                            <span><img src="{{ asset('/') }}assets/img/icons/car-parts-01.svg" alt="Diesel"></span>
                                                            <p class="text-capitalize">{{$car->transmission}}</p>
                                                        </li>
                                                        <li>
                                                            <span><img src="{{ asset('/') }}assets/img/icons/car-parts-05.svg" alt="2018"></span>
                                                            <p>2018</p>
                                                        </li>
                                                        <li>
                                                            <span><img src="{{ asset('/') }}assets/img/icons/car-parts-04.svg" alt="Petrol"></span>
                                                            <p>{{$car->type->name}}</p>
                                                        </li>
                                                        <li>
                                                            <span><img src="{{ asset('/') }}assets/img/icons/car-parts-06.svg" alt="Petrol"></span>
                                                            <p>{{$car->stock}} Mobil</p>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="blog-list-head list-head-bottom d-flex">
                                                    <div class="blog-list-title">
                                                        <div class="title-bottom d-flex align-items-center">
                                                            <div class="car-list-icon">
                                                                <img src="{{ asset($car->brand->logo) }}" alt>
                                                            </div>
                                                            <div class="address-info">
                                                                <h5><a href>{{$car->brand->name}}</a></h5>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="listing-button">
                                                        <a href="#" type="sumit" class="btn btn-order">Lihat Mobil</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>

                    {{$cars->links('vendor.pagination.custom')}}

                </div>
            </div>
        </div>
    </section>
@endsection
