@extends('layouts.app')

@section('content')
    <div class="breadcrumb-bar">
        <div class="container">
            <div class="row align-items-center text-center">
                <div class="col-md-12 col-12">
                    <h2 class="breadcrumb-title">Kontak Kami</h2>
                    <nav aria-label="breadcrumb" class="page-breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{route('homepage')}}">Beranda</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Kontak Kami</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>

    <section class="contact-section">
        <div class="container">
            <div class="contact-info-area">
                <div class="row justify-content-center">
                    <div class="col-lg-3 col-md-6 col-12 d-flex aos-init aos-animate" data-aos="fade-down" data-aos-duration="1200" data-aos-delay="0.1">
                        <div class="single-contact-info flex-fill">
                            <span><i class="fa fa-phone"></i></span>
                            <h3>Nomor Telepon / WA</h3>
                            <a href="tel:(888)888-8888">+62 821 2585 7754</a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-12 d-flex aos-init aos-animate" data-aos="fade-down" data-aos-duration="1200" data-aos-delay="0.2">
                        <div class="single-contact-info flex-fill">
                            <span><i class="fa fa-envelope"></i></span>
                            <h3>Alamat Email</h3>
                            <a href="">
                                ahmad.maulana002@binus.ac.id
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-12 d-flex aos-init aos-animate" data-aos="fade-down" data-aos-duration="1200" data-aos-delay="0.3">
                        <div class="single-contact-info flex-fill">
                            <span><i class="fa fa-map-marked"></i></span>
                            <h3>Lokasi</h3>
                            <a href="javascript:void(0);">
                                Jl Raya Cibogo No.20 Subang, Jawa Barat
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-info-area aos-init aos-animate" data-aos="fade-down" data-aos-duration="1200" data-aos-delay="0.5">
                <div class="row">
                    <div class="col-lg-6">
                        <img src="assets/img/contact-info.jpg" class="img-fluid" alt="Contact">
                    </div>
                    <div class="col-lg-6">
                        <form action="#">
                            <div class="row">
                                <h1>Hubungi Kami!</h1>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Nama <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" placeholder="">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Alamat Email <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" placeholder="">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Nomor Telepon <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" placeholder="">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Pesan <span class="text-danger">*</span></label>
                                        <textarea class="form-control" rows="4" cols="50" placeholder="">											</textarea>
                                    </div>
                                </div>
                            </div>
                            <button class="btn contact-btn">Kirim</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

