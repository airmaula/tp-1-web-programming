@extends('layouts.app')

@section('content')
    <section class="section" style="min-height: 70vh">
        <div class="section-heading aos-init aos-animate" data-aos="fade-down">
            <h2>Jadwal Pelatihan</h2>
            <p>Tentukan waktu terbaikmu untuk belajar mengemudi</p>
        </div>
        <div class="container">

            @foreach($schedules->chunk(5) as $data)
                <div class="row justify-content-center">

                    @foreach($data as $schedule)
                        <div class="col-2">
                            <div class="card">
                                <div class="card-body text-center">
                                    <h3 class="mb-2">{{$days[$schedule->day-1]}}</h3>
                                    <p>{{substr($schedule->start, 0, 5)}} - {{substr($schedule->end, 0, 5)}}</p>
                                </div>
                            </div>
                        </div>
                    @endforeach

                </div>
            @endforeach
        </div>
    </section>
@endsection
