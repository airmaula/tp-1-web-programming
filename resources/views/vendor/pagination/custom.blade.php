@if ($paginator->hasPages())
<div class="blog-pagination">
    <nav>
        <ul class="pagination page-item justify-content-center">

            @if ($paginator->onFirstPage())

            @else
                <li class="previtem">
                    <a class="page-link" href="{{ $paginator->previousPageUrl() }}"><i class="fas fa-regular fa-arrow-left me-2"></i> Prev</a>
                </li>
            @endif

            <li class="justify-content-center pagination-center">
                <div class="page-group">
                    <ul>

                        @foreach ($elements as $element)
                            {{-- "Three Dots" Separator --}}
                            @if (is_string($element))
                                <li class="page-item disabled" aria-disabled="true"><span class="page-link">{{ $element }}</span></li>
                            @endif

                            {{-- Array Of Links --}}
                            @if (is_array($element))
                                @foreach ($element as $page => $url)
                                    @if ($page == $paginator->currentPage())
                                        <li class="page-item">
                                            <a class="active page-link" href="{{ $url }}">{{ $page }}  <span class="visually-hidden">(current)</span></a>
                                        </li>
                                    @else
                                        <li class="page-item">
                                            <a class="page-link" href="{{ $url }}">{{ $page }}</a>
                                        </li>
                                    @endif
                                @endforeach
                            @endif
                        @endforeach

                    </ul>
                </div>
            </li>

            @if ($paginator->hasMorePages())
                <li class="nextlink">
                    <a class="page-link" href="{{ $paginator->nextPageUrl() }}">Next <i class="fas fa-regular fa-arrow-right ms-2"></i></a>
                </li>
            @else

            @endif

        </ul>
    </nav>
</div>
@endif
