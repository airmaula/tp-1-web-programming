<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
 * Page: Homepage
 */
Route::get('/', [\App\Http\Controllers\FrontController::class, 'homepage'])->name('homepage');

/*
 * Page: Schedule
 */
Route::get('/schedule', [\App\Http\Controllers\FrontController::class, 'schedule'])->name('schedule');

/*
 * Page: Plan
 */
Route::get('/plan', [\App\Http\Controllers\FrontController::class, 'plan'])->name('plan');

/*
 * Page: Car Listing
 */
Route::get('/cars', [\App\Http\Controllers\FrontController::class, 'cars'])->name('cars');

/*
 * Page: Trainer & Staff
 */
Route::get('/our-team', [\App\Http\Controllers\FrontController::class, 'staff'])->name('staff');

/*
 * Page: Contact
 */
Route::get('/contact', [\App\Http\Controllers\FrontController::class, 'contact'])->name('contact');
