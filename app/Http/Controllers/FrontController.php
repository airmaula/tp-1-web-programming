<?php

namespace App\Http\Controllers;

use App\Models\Car;
use App\Models\CarBrand;
use App\Models\CarType;
use App\Models\Plan;
use App\Models\Schedule;
use App\Models\Staff;
use Illuminate\Http\Request;

class FrontController extends Controller
{

    function __construct()
    {
        $this->middleware('throttle:rate_limit,200');
    }

    public function homepage()
    {

        // mengambil data dari database menggunakan eloquent
        $brands = CarBrand::all();
        $types  = CarType::all();
        $plans  = Plan::all();

        // menampilkan view dari file resources/views/pages/homepage.blade.php
        return view('pages.homepage', compact('brands', 'types', 'plans'));

    }

    public function schedule()
    {
        // mengambil data jadwal kursus dari tabel schedule
        $schedules = Schedule::all();
        $days = ['Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu', 'Minggu'];

        // menampilkan view dari file resources/views/pages/schedule.blade.php
        return view('pages.schedule', compact('schedules', 'days'));
    }

    public function plan()
    {
        // mengambil data jadwal kursus dari tabel plans
        $plans = Plan::all();

        // menampilkan view dari file resources/views/pages/plan.blade.php
        return view('pages.plan', compact('plans'));
    }

    public function cars(Request $request)
    {
        // mengambil data dari database menggunakan eloquent
        $brands = CarBrand::all();
        $types  = CarType::all();

        // mengambil data mobil berdasarkan yang dicari
        $cars = Car::where(function ($query) use ($request) {
            if ($request->get('keyword')) {
                $query->where('name', 'like', '%'. $request->get('keyword') .'%');
            }

            if ($request->get('brands')) {
                $query->whereIn('brand_id', $request->get('brands'));
            }

            if ($request->get('types')) {
                $query->whereIn('type_id', $request->get('types'));
            }
        })->paginate(4);

        // menampilkan view dari file resources/views/pages/cars.blade.php
        return view('pages.cars', compact('brands', 'types', 'cars'));
    }

    public function staff()
    {
        // mengambil data pengurus dan instruktur dari tabel staff
        $staff = Staff::all()->groupBy('role')->reverse();

        // menampilkan view dari file resources/views/pages/staff.blade.php
        return view('pages.staff', compact('staff'));
    }

    public function contact()
    {
        // menampilkan view dari file resources/views/pages/contact.blade.php
        return view('pages.contact');
    }
}
