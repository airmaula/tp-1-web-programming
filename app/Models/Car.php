<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    use HasFactory;

    public function brand()
    {
        return $this->belongsTo(CarBrand::class, 'brand_id', 'id');
    }

    public function type()
    {
        return $this->belongsTo(CarType::class, 'type_id', 'id');
    }
}
